
# Bucket Drumming

Here I save my bucket drum basics sheets and my choreo sheets. Created with musescore, some files also exported as PDF and translated to German. Visit my musescore profile to listen to the transcriptions in Musescore's player: https://musescore.com/user/49641323
